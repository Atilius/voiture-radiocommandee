# Voiture Radiocommandee

## Composants

* Batterie : HRB Batterie Lipo RC 7.4V 6000mAh 60C 2S Batterie avec XT60 Plug et Spare Plugs Deans T pour Racing Drone Voiture Camion Bateau Hélicoptère Quadcopt

* Moteurs Brushless : Crazepony-UK 3670 2150KV 5mm Sensorless Brushless Motor with 80A Splashproof ESC (Electric Speed Controller) for 1/8 RC Car Truck Running Off-Road Veh
* Télécommande et receiver : Transmetteur RC AFHDS Flysky FS-i6X 10CH 2,4 GHz avec récepteur FS-iA6B
* Pneus : huhushop Jantes Caoutchouc RC Racing YOSOO (TM) 4 pour Jantes Auto HSP HPI 9068-6081 1/10 sur la Route
* Servomoteur : FS5323M
* Microcontrôleur : Arduino Uno

## Branchement

### Arduino

#### DIGITAL

* 0  :  RX
* 1  :  TX
* 2  :  Signal moteur droit
* 3~ :  Signal relais activation/désactivation moteur
* 4  :  Signal moteur gauche
* 5~ :
* 6~ :  Eclairage
* 7  :  Servomoteur direction
* 8  :  Signal Channel 1
* 9~ :  Signal Channel 2
* 10~:  Signal Channel 3
* 11~:  Signal Channel 4
* 12 :  Signal Channel 5
* 13 :  Signal Channel 6

#### POWER

* Vcc :  LiPo
* GND :  GND receiver
* GND :  GND moteur droit/GND moteur gauche
* GND :  GND CaptTempérature
* 5V  :  CaptTempérature/Eclairage

#### ANALOGIQUE

* 0 : Capteur de température

### RECEIVER

* CH1 : pin digital 8
* CH2 : pin digital 9
* CH3 : pin digital 10
* CH4 : pin digital 11
* CH5 : pin digital 12
* CH6 : pin digital 13
* GND : GND Arduino
* Vcc : LiPo

### MOTEUR

* S :
  * droit  : pin digital 2
  * gauche : pin digital 4
* GND : GND Arduino
* 6V  : Vcc Servomoteur
* Vcc : LiPo

### Servo direction

* S  : pin digital 7
* GND: GND moteur
* Vcc: 6V moteur
