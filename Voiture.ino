/* Definitions des pin

    ---------------Arduino---------------

    DIGITAL
    0  :  RX
    1  :  TX
    2  :  Signal moteur droit
    3~ :  Signal relais activation/désactivation moteur
    4  :  Signal moteur gauche
    5~ :
    6~ :  Eclairage
    7  :  Servomoteur direction

    8  :  Signal Channel 1
    9~ :  Signal Channel 2
    10~:  Signal Channel 3
    11~:  Signal Channel 4
    12 :  Signal Channel 5
    13 :  Signal Channel 6

    POWER
    Vcc :  LiPo
    GND :  GND receiver
    GND :  GND moteur droit/GND moteur gauche
    GND :  GND CaptTempérature
    5V  :  CaptTempérature/Eclairage

    ANALOGIQUE
    0 : Capteur de température

    ------------RECEIVER------------

    CH1 : pin digital 8
    CH2 : pin digital 9
    CH3 : pin digital 10
    CH4 : pin digital 11
    CH5 : pin digital 12
    CH6 : pin digital 13
    GND : GND Arduino
    Vcc : LiPo

    -----------MOTEUR-----------

    S   :
          droit  : pin digital 2
          gauche : pin digital 4
    GND : GND Arduino
    6V  : Vcc Servomoteur
    Vcc : LiPo

    -------Servo direction-------

    S  : pin digital 7
    GND: GND moteur
    Vcc: 6V moteur
*/

//----------------------------------

#define PINTEMP       0
#define PINMOTDROIT   2
#define PINMOTEUR     2
#define PINRELAIS     3
#define PINMOTGAUCHE  4
#define PINLUMIERE    6
#define PINSERVODIR   7


#define CHANNEL1      0
#define CHANNEL2      1
#define CHANNEL3      2
#define CHANNEL4      3
#define CHANNEL5      4
#define CHANNEL6      5

#define YAW           0
#define PITCH         1
#define THROTTLE      2
#define ROLL          3
#define VRA           4
#define SWC           5
#define TIME          6

#define LUMIEREAVGA   0
#define LUMIEREAVDR   1
#define LUMIEREARDR   2
#define LUMIEREARGA   3

#define VALBRIDAGE    1650
#define TPSVITESSEMAX 1000
#define NBRMOTEUR     2
#define DIFFERENTIEL  2
#define TPSORIENTATION 10

#define TEMPMAX       50
#define TEMPREPRISE   20

#define MIN           0
#define NEUTRE        75
#define MAX           180

#define AFFICHAGE     1

#define NBRLUMIERE    4
#define DELAIINCRE    3

#define DELAISRELAIS  1000

#define TIMEFLOOD     20

//-----------------------------------

#include <Servo.h>

#include <Adafruit_NeoPixel.h>
#ifdef __AVR__
#include <avr/power.h>
#endif

//-----------------------------------

Servo servoDirection;

#if NBRMOTEUR == 2
Servo motDroit;
Servo motGauche;
#elif NBRMOTEUR == 1
Servo mot;
#endif

Adafruit_NeoPixel pixels = Adafruit_NeoPixel(NBRLUMIERE, PINLUMIERE, NEO_GRB + NEO_KHZ800);

//----------------------------------

volatile unsigned long current_time;
volatile unsigned long timer[7];

volatile byte previous_state[6];

volatile unsigned int pulse_duration[6] = {1500, 1500, 1500, 1500, 1500, 1500};

//int mode_mapping[6];

#if AFFICHAGE == 1
char controle[6][10] = {"Roulis", "Pas", "Gaz", "Lacet", "VrA", "SwitchC"};
#endif

int gazD, gazG, vitesse, vitesseBridee = 0;
int orientation = 90;
int etatPrecedant;
int valTemperature;

bool sens = 1;
bool vitesseMax = 0;
bool lumiere = 0;
bool esc = 0;

int i;

/******************VOID SETUP******************/
void setup() {

  //#if AFFICHAGE == 1
  Serial.begin(9600);
  //#endif

  pixels.begin();

  pinMode( 3, OUTPUT);

  analogReference(INTERNAL);

  /*
    mode_mapping[YAW]      = CHANNEL4;
    mode_mapping[PITCH]    = CHANNEL2;
    mode_mapping[ROLL]     = CHANNEL1;
    mode_mapping[THROTTLE] = CHANNEL3;
    mode_mapping[VRA]      = CHANNEL5;
    mode_mapping[SWC]      = CHANNEL6;
  */

  PCICR  |= (1 << PCIE0);
  PCMSK0 |= (1 << PCINT0);
  PCMSK0 |= (1 << PCINT1);
  PCMSK0 |= (1 << PCINT2);
  PCMSK0 |= (1 << PCINT3);
  PCMSK0 |= (1 << PCINT4);
  PCMSK0 |= (1 << PCINT5);

  servoDirection.attach(PINSERVODIR, 500, 2500);
  servoDirection.write(90);

#if NBRMOTEUR == 2
  motDroit.attach(PINMOTDROIT, 1000, 2000);
  motGauche.attach(PINMOTGAUCHE, 1000, 2000);
  motDroit.write(NEUTRE);
  motGauche.write(NEUTRE);
#elif NBRMOTEUR == 1
  mot.attach(PINMOTEUR, 1000, 2000);
  mot.write(NEUTRE);
#endif

  delay(100);
  sw();
  delay(100);
  clignote(3, 0, 255, 0);

}
/*************************VOID LOOP***********************/
void loop() {

#if AFFICHAGE == 1
  affichageCanaux();
#endif
  moteur();
  vitesseMaximum();
  bridage();
  direction_voiture();
  verificationTemperature();
  sw();

  if (pulse_duration[ROLL] < 1200 && lumiere == 0)
    allumageLumiere();
  else if (pulse_duration[ROLL] < 1200 && lumiere == 1)
    extinctionLumiere();

  delay(TIMEFLOOD);

}

/******************SWITCH******************/
void sw()
{
  if ( pulse_duration[SWC] < 1250 )
  {
    
    servoDirection.write(90);
#if NBRMOTEUR == 2
    motDroit.write(NEUTRE);
    motGauche.write(NEUTRE);
#elif NBRMOTEUR == 1
    mot.write(NEUTRE);
#endif

    while (pulse_duration[SWC] < 1250 )
      clignote(1, 255, 0, 0);
  }
  else if ( pulse_duration > 1750 && esc == 2)
  {
    if ( esc == 0 )
      activationESC( );
      
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
  }
  else
  {
    if ( esc == 1 )
      desactivationESC( );
  }
}
/*************************TEMPERATURE*************************/
int temperature()
{
  valTemperature = analogRead(PINTEMP) * 110 / 1023;
  return valTemperature;
}

/*************************TEMPERATURE DE SECURITE*************************/
void verificationTemperature()
{
  if ( temperature() >= TEMPMAX )
  {
    while ( temperature() > TEMPREPRISE )
    {

#if NBRMOTEUR == 2
      motDroit.write(NEUTRE);
      motGauche.write(NEUTRE);
#elif NBRMOTEUR == 1
      mot.write(NEUTRE);
#endif

    }
  }
}
/*********************ACTIVATIONS DES ESC*********************/
void activationESC( )
{
  digitalWrite ( PINRELAIS , HIGH );
  delay ( DELAISRELAIS );
  digitalWrite ( PINRELAIS , LOW );
}

/*********************DESACTIVTION DES ESC*********************/
void desactivationESC( )
{
  digitalWrite ( PINRELAIS , HIGH );
  delay ( DELAISRELAIS );
  digitalWrite ( PINRELAIS , LOW );
}


/************************LUMIERE***********************/
void clignote( int j , int rouge, int vert, int bleu)
{

  for ( i = 1 ; i <= j ; i++)
  {
    pixels.setPixelColor(LUMIEREAVGA, pixels.Color(0, 0, 0));
    pixels.setPixelColor(LUMIEREAVDR, pixels.Color(0, 0, 0));
    pixels.setPixelColor(LUMIEREARDR, pixels.Color(0, 0, 0));
    pixels.setPixelColor(LUMIEREARGA, pixels.Color(0, 0, 0));
    pixels.show();
    delay(500);

    pixels.setPixelColor(LUMIEREAVGA, pixels.Color(vert, rouge, bleu));
    pixels.setPixelColor(LUMIEREAVDR, pixels.Color(vert, rouge, bleu));
    pixels.setPixelColor(LUMIEREARDR, pixels.Color(vert, rouge, bleu));
    pixels.setPixelColor(LUMIEREARGA, pixels.Color(vert, rouge, bleu));
    pixels.show();
    delay(500);
  }

  pixels.setPixelColor(LUMIEREAVGA, pixels.Color(0, 0, 0));
  pixels.setPixelColor(LUMIEREAVDR, pixels.Color(0, 0, 0));
  pixels.setPixelColor(LUMIEREARDR, pixels.Color(0, 0, 0));
  pixels.setPixelColor(LUMIEREARGA, pixels.Color(0, 0, 0));
  pixels.show();
}

void allumageLumiere()
{
  lumiere = 1;
  for ( i = 0 ; i <= 255 ; i++)
  {
    //pixels.Color(v, r, b)
    pixels.setPixelColor(LUMIEREAVGA, pixels.Color(i, i, i));
    pixels.setPixelColor(LUMIEREAVDR, pixels.Color(i, i, i));
    pixels.setPixelColor(LUMIEREARDR, pixels.Color(0, i, 0));
    pixels.setPixelColor(LUMIEREARGA, pixels.Color(0, i, 0));
    pixels.show();
    delay(DELAIINCRE);
  }
}
//----------------------------------------------------
void extinctionLumiere()
{
  lumiere = 0;
  for ( i = 255 ; i >= 0 ; i--)
  {
    //pixels.Color(v, r, b)
    pixels.setPixelColor(LUMIEREAVGA, pixels.Color(i, i, i));
    pixels.setPixelColor(LUMIEREAVDR, pixels.Color(i, i, i));
    pixels.setPixelColor(LUMIEREARDR, pixels.Color(0, i, 0));
    pixels.setPixelColor(LUMIEREARGA, pixels.Color(0, i, 0));
    pixels.show();
    delay(DELAIINCRE);
  }
}

/*************************MOTEUR*************************/

void moteur()
{

  //---------verification du sens---------

  if (pulse_duration[PITCH] < 1200)
    sens = 0;
  else if (pulse_duration[PITCH] > 1700)
    sens = 1;

  //---------neutre---------

  if (pulse_duration[THROTTLE] <= 1050)
  {
#if NBRMOTEUR == 2
    gazD = NEUTRE;
    gazG = NEUTRE;
    motDroit.write(gazD);
    motGauche.write(gazG);
#elif NBRMOTEUR == 1
    vitesse = NEUTRE;
    mot.write(vitesse);
#endif
  }
  //---------acceleration---------
  else
  {
    //---------marche arrière---------
    if (sens == 0)
    {
      vitesse = map(pulse_duration[THROTTLE], 1050, 2000, 65, 0) + vitesseBridee;
      gazD = vitesse;
      gazG = gazD;
#if NBRMOTEUR == 2
      motDroit.write(gazD);
      motGauche.write(gazG);
#elif NBRMOTEUR == 1
      mot.write(vitesse);
#endif
    }
    //---------marche avant---------
    else if (sens == 1)
    {
#if NBRMOTEUR == 2


      gazD = map(pulse_duration[THROTTLE], 1050, 2000, 85, 180) - vitesseBridee;
      gazG = gazD;

      if (pulse_duration[YAW] < 1488)
      {
        gazD *= map(pulse_duration[YAW], 1050, 1488, DIFFERENTIEL, 1);
        gazD = constrain(gazD, 85, 180);
      }
      motDroit.write(gazD);

      if (pulse_duration[YAW] > 1512)
      {
        gazG *= map(pulse_duration[YAW], 1512, 1996, 1, DIFFERENTIEL);
        gazG = constrain(gazG, 85, 180);
      }
      motGauche.write(gazG);

#elif NBRMOTEUR == 1

      if ( pulse_duration[VRA] >= VALBRIDAGE )
        vitesse = map(pulse_duration[THROTTLE], 1050, 2000, 85, vitesseBridee);
      else
        vitesse = map(pulse_duration[THROTTLE], 1050, 2000, 85, 180);
      mot.write(vitesse);

#endif
    }
  }
}
/************************BRIDAGE************************/
void bridage()
{
  if ( pulse_duration[VRA] >= VALBRIDAGE )
  {
    vitesseBridee = map(pulse_duration[VRA], VALBRIDAGE, 2000, 180, 85);
  }
  else
  {
    vitesseBridee = 0;
  }
}

/************************VITESSE MAXIMUM************************/
void vitesseMaximum()
{
  //-------------Vitesse Max-------------
  if (  pulse_duration[VRA] >= 1200  && vitesseMax != 0)
    vitesseMax = 0;

  if (pulse_duration[VRA] < 1200 && vitesseMax == 0 && pulse_duration[THROTTLE] > 1100 )
  {
    vitesseMax = 1;

    gazD    = MAX;
    gazG    = MAX;
    vitesse = MAX;

#if NBRMOTEUR == 2
    motDroit.write(gazD);
    motGauche.write(gazG);
#elif NBRMOTEUR == 1
    mot.write(vitesse);
#endif

    timer[TIME] = millis();

    do {
      current_time = millis();
    }
    while ( current_time - timer[TIME] < TPSVITESSEMAX );

  }
}

/*************************DIRECTION*************************/
void direction_voiture()
{
  orientation = map(pulse_duration[YAW], 1000, 2000, 62, 140);

  if (etatPrecedant < orientation)
    for ( i = etatPrecedant ; i != orientation ; i++)
    {
      servoDirection.write(i);
      delay(TPSORIENTATION);
    }
  else if (etatPrecedant > orientation)
    for ( i = etatPrecedant ; i != orientation ; i--)
    {
      servoDirection.write(i);
      delay(TPSORIENTATION);
    }
  etatPrecedant = orientation;
}

/************************AFFICHAGE************************/

#if AFFICHAGE == 1
void affichageCanaux()
{
  for ( i = CHANNEL1; i <= CHANNEL6; i++)
  {

    //    Serial.print("Channel ");
    //    Serial.print(i+1);

    Serial.print(controle[i]);
    Serial.print(": ");
    Serial.print(pulse_duration[i]);
    Serial.print("  ");
  }
  Serial.print("Sens: ");
  Serial.print(sens);

#if NBRMOTEUR == 2
  Serial.print("  motD: ");
  Serial.print(gazD);
  Serial.print("  motG: ");
  Serial.print(gazG);
#elif NBRMOTEUR == 1
  Serial.print("  vitesse: ");
  Serial.print(vitesse);
#endif

  Serial.print("  temp: ");
  Serial.print(temperature());

  Serial.print("  dir: ");
  Serial.print(orientation);

  Serial.print("  brid: ");
  Serial.print(vitesseBridee);

  Serial.print("  lum: ");
  Serial.print(lumiere);

  Serial.print("\n");
}
#endif

/***********************INTERRUPTIONS**********************/

ISR(PCINT0_vect)
{
  current_time = micros();

  //Channel 1 ------------------------------------------------------------------------------------
  if (PINB & B00000001) {
    if (previous_state[CHANNEL1] == LOW) {
      previous_state[CHANNEL1] = HIGH;
      timer[CHANNEL1]          = current_time;
    }
  } else if (previous_state[CHANNEL1] == HIGH) {
    previous_state[CHANNEL1] = LOW;
    pulse_duration[CHANNEL1] = current_time - timer[CHANNEL1];
  }

  //Channel 2 ------------------------------------------------------------------------------------
  if (PINB & B00000010) {
    if (previous_state[CHANNEL2] == LOW) {
      previous_state[CHANNEL2] = HIGH;
      timer[CHANNEL2]          = current_time;
    }
  } else if (previous_state[CHANNEL2] == HIGH) {
    previous_state[CHANNEL2] = LOW;
    pulse_duration[CHANNEL2] = current_time - timer[CHANNEL2];
  }

  //Channel 3 ------------------------------------------------------------------------------------
  if (PINB & B00000100) {
    if (previous_state[CHANNEL3] == LOW) {
      previous_state[CHANNEL3] = HIGH;
      timer[CHANNEL3]          = current_time;
    }
  } else if (previous_state[CHANNEL3] == HIGH) {
    previous_state[CHANNEL3] = LOW;
    pulse_duration[CHANNEL3] = current_time - timer[CHANNEL3];
  }

  //Channel 4 ------------------------------------------------------------------------------------
  if (PINB & B00001000) {
    if (previous_state[CHANNEL4] == LOW) {
      previous_state[CHANNEL4] = HIGH;
      timer[CHANNEL4]          = current_time;
    }
  } else if (previous_state[CHANNEL4] == HIGH) {
    previous_state[CHANNEL4] = LOW;
    pulse_duration[CHANNEL4] = current_time - timer[CHANNEL4];
  }

  //Channel 5 ------------------------------------------------------------------------------------
  if (PINB & B00010000) {
    if (previous_state[CHANNEL5] == LOW) {
      previous_state[CHANNEL5] = HIGH;
      timer[CHANNEL5]          = current_time;
    }
  } else if (previous_state[CHANNEL5] == HIGH) {
    previous_state[CHANNEL5] = LOW;
    pulse_duration[CHANNEL5] = current_time - timer[CHANNEL5];
  }

  //Channel 6 ------------------------------------------------------------------------------------
  if (PINB & B00100000) {
    if (previous_state[CHANNEL6] == LOW) {
      previous_state[CHANNEL6] = HIGH;
      timer[CHANNEL6]          = current_time;
    }
  } else if (previous_state[CHANNEL6] == HIGH) {
    previous_state[CHANNEL6] = LOW;
    pulse_duration[CHANNEL6] = current_time - timer[CHANNEL6];
  }
}


//    ***************************************************
//   *                                                 **
//  *                                                 * *
// *                                                 *  *
//***************************************************   *
//*                                                 *   *
//*                     FIN DU                      *  *
//*                      CODE                       * *
//*                                                 **
//***************************************************
